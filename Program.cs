﻿using ticTacToe.Handlers;

namespace ticTacToe
{
    static class Program
    {
        static void Main(string[] args)
        {
            Game.Start();
        }
    }
}
