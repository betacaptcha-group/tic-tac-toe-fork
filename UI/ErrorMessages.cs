﻿using System;

namespace ticTacToe.UI
{
    public static class ErrorMessages
    {
        public static void WrongGridSizeMessage()
        {
            Console.WriteLine("Неправильный размер\n");
        }
        public static void WrongMenuOptionNumber()
        {
            Console.WriteLine("Введите номер из пункта меню\n");
        }
    }
}
