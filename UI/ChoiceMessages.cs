﻿using System;

namespace ticTacToe.UI
{
    public static class ChoiceMessages
    {
        public static void GridSizeSelectionMessage()
        {
            Console.Write("Введите размер игрового поля(нечётное число не меньше 3): ");
        }
        public static void GameModeSelectionMessage()
        {
            Console.WriteLine($"Выберите режим игры:\n1.Игра с другим человеком\n2.Игра с ботом\n3.Назад");
        }
        public static void SignSelectionMessage()
        {
            Console.WriteLine($"Выберите, чем ходить:\n1.x\n2.o");
        }
        public static void FirstPlayerSelectionMessage()
        {
            Console.WriteLine($"Кто ходит первым?\n1.x\n2.o\n3.Определить случайно");
        }
        public static void CellSelectionMessage(bool isGameWithBot, char activeSign)
        {
            if (isGameWithBot)
            {
                Console.Write($"\nВыберите клетку(используйте клавиши W, A, S, D для перемещения и Enter для подтверждения):");
            }
            else
            {
                Console.Write($"\nХодит {activeSign}\n\nВыберите клетку(используйте клавиши W, A, S, D для перемещения и Enter для подтверждения):");
            }
        }
    }
}
