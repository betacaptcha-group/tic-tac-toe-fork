﻿using System;

namespace ticTacToe.UI
{
    public static class GameResult
    {
        public static void Print(int xCombo, int oCombo)
        {
            string gameResult;
            if (xCombo > oCombo)
            {
                gameResult = "Победил x";
            }
            else if (oCombo > xCombo)
            {
                gameResult = "Победил o";
            }
            else
            {
                gameResult = "Ничья";
            }
            Console.WriteLine($"Комбинации x: {xCombo}\nКомбинации o: {oCombo}\n\n{gameResult}");
        }
    }
}
