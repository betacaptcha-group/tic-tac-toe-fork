﻿using System;

namespace ticTacToe.UI
{
    public static class Grid
    {
        private const string underline = "\x1B[4m";
        private const string reset = "\x1B[0m";
        public static void Print(char[,] cells, int gridSize)
        {
            Console.WriteLine(underline + new string(' ', gridSize * 2) + reset);
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    char cell = cells[i, j] == '\0' ? ' ' : cells[i, j];
                    Console.Write($"{underline}{cell}|{reset}");
                }
                Console.WriteLine();
            }
        }
    }
}
