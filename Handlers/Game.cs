﻿using System;
using System.Linq;
using ticTacToe.Models;
using ticTacToe.UI;

namespace ticTacToe.Handlers
{
    public static class Game
    {
        public static void Start()
        {
            try
            {
                ChoiceMessages.GridSizeSelectionMessage();
                GameParametrs.GridSize = Convert.ToInt32(Console.ReadLine());
                if (GameParametrs.GridSize % 2 == 0)
                {
                    throw new ArgumentException(nameof(GameParametrs.GridSize));
                }

                GameParametrs.Cells = new char[GameParametrs.GridSize, GameParametrs.GridSize];
                GameParametrs.IsCellFilled = new bool[GameParametrs.GridSize, GameParametrs.GridSize];
            }
            catch
            {
                GameSpace.Clear();
                ErrorMessages.WrongGridSizeMessage();
                Start();
            }
            GameSpace.Clear();
            Menu.GameModeSelection();
        }
        public static void Gameplay()
        {
            CheckForGameOver();
            Random random = new Random();
            Grid.Print(GameParametrs.Cells, GameParametrs.GridSize);
            char activeSign = GameParametrs.IsCrossFirst ? GameParametrs.xSign : GameParametrs.oSign;
            ChoiceMessages.CellSelectionMessage(GameParametrs.IsGameWithBot, activeSign);
            try
            {
                int row;
                int column;
                if (!GameParametrs.IsGameWithBot || GameParametrs.PlayerSign == activeSign)
                {
                    CellSelection.WASDCellSelect(GameParametrs.GridSize, out row, out column);
                }
                else
                {
                    row = random.Next(GameParametrs.GridSize + 1);
                    column = random.Next(GameParametrs.GridSize + 1);
                }

                if (!GameParametrs.IsCellFilled[row, column])
                {
                    GameParametrs.Cells[row, column] = activeSign;
                    GameParametrs.IsCellFilled[row, column] = true;
                    GameParametrs.IsCrossFirst = !GameParametrs.IsCrossFirst;
                }
            }
            catch
            {
                GameSpace.Clear();
                Gameplay();
            }

            Combos.Check();
            GameSpace.Clear();
            Gameplay();
        }
        private static void CheckForGameOver()
        {
            string str = string.Concat(GameParametrs.Cells.Cast<char>().ToArray());
            int emptyCells = str.Count(cell => cell == '\0');
            if (emptyCells == 0 || GameParametrs.XComboCounter > emptyCells / 2 || GameParametrs.OComboCounter > emptyCells / 2)
            {
                GameResult.Print(GameParametrs.XComboCounter, GameParametrs.OComboCounter);
                Environment.Exit(1);
            }
        }
    }
}
