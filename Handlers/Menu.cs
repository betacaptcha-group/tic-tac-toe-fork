﻿using System;
using ticTacToe.Models;
using ticTacToe.UI;

namespace ticTacToe.Handlers
{
    public static class Menu
    {
        public static void GameModeSelection()
        {
            ChoiceMessages.GameModeSelectionMessage();
            switch (Console.ReadLine())
            {
                case "1":
                    GameSpace.Clear();
                    FirstPlayerSelection();
                    Game.Gameplay();
                    break;
                case "2":
                    GameSpace.Clear();
                    GameParametrs.IsGameWithBot = true;
                    SignSelection();
                    FirstPlayerSelection();
                    Game.Gameplay();
                    break;
                case "3":
                    GameSpace.Clear();
                    Game.Start();
                    break;
                default:
                    GameSpace.Clear();
                    ErrorMessages.WrongMenuOptionNumber();
                    GameModeSelection();
                    break;
            }
        }
        public static void SignSelection()
        {
            ChoiceMessages.SignSelectionMessage();
            switch (Console.ReadLine())
            {
                case "1":
                    GameParametrs.PlayerSign = GameParametrs.xSign;
                    break;
                case "2":
                    GameParametrs.PlayerSign = GameParametrs.oSign;
                    break;
                default:
                    GameSpace.Clear();
                    ErrorMessages.WrongMenuOptionNumber();
                    SignSelection();
                    break;
            }
            GameSpace.Clear();
        }
        private static void FirstPlayerSelection()
        {
            ChoiceMessages.FirstPlayerSelectionMessage();
            switch (Console.ReadLine())
            {
                case "1":
                    GameParametrs.IsCrossFirst = true;
                    break;
                case "2":
                    GameParametrs.IsCrossFirst = false;
                    break;
                case "3":
                    RandomFirstPlayerSelection();
                    break;
                default:
                    GameSpace.Clear();
                    ErrorMessages.WrongMenuOptionNumber();
                    FirstPlayerSelection();
                    break;
            }
            GameSpace.Clear();
        }
        private static void RandomFirstPlayerSelection()
        {
            Random random = new Random();
            GameParametrs.IsCrossFirst = Convert.ToBoolean(random.Next(1));
        }
    }
}
