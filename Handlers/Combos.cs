﻿using System.Text;
using System.Text.RegularExpressions;
using ticTacToe.Models;

namespace ticTacToe.Handlers
{
    public static class Combos
    {
        public static void Check()
        {
            GameParametrs.XComboCounter = 0;
            GameParametrs.OComboCounter = 0;
            string xComboPattern = new string(GameParametrs.xSign, 3);
            string oComboPattern = new string(GameParametrs.oSign, 3);
            StringBuilder row = new StringBuilder();
            StringBuilder column = new StringBuilder();
            StringBuilder diagonal1 = new StringBuilder();
            StringBuilder diagonal2 = new StringBuilder();
            int diagonalCounter = GameParametrs.GridSize - 3;
            for (int i = 0; i < GameParametrs.GridSize; i++)
            {
                for (int j = 0; j < GameParametrs.GridSize; j++)
                {
                    row.Append(GameParametrs.Cells[i, j]);
                    column.Append(GameParametrs.Cells[j, i]);
                    if (j + diagonalCounter >= 0 && j + diagonalCounter < GameParametrs.GridSize)
                    {
                        diagonal1.Append(GameParametrs.Cells[j, j + diagonalCounter]);
                        diagonal2.Append(GameParametrs.Cells[j, GameParametrs.GridSize - j - diagonalCounter - 1]);
                    }
                }
                if (diagonalCounter >= 2 - GameParametrs.GridSize)
                {
                    diagonalCounter--;
                }

                GameParametrs.XComboCounter += new Regex(xComboPattern).Matches(row.ToString()).Count;
                GameParametrs.XComboCounter += new Regex(xComboPattern).Matches(column.ToString()).Count;
                GameParametrs.XComboCounter += new Regex(xComboPattern).Matches(diagonal1.ToString()).Count;
                GameParametrs.XComboCounter += new Regex(xComboPattern).Matches(diagonal2.ToString()).Count;
                GameParametrs.OComboCounter += new Regex(oComboPattern).Matches(row.ToString()).Count;
                GameParametrs.OComboCounter += new Regex(oComboPattern).Matches(column.ToString()).Count;
                GameParametrs.OComboCounter += new Regex(oComboPattern).Matches(diagonal1.ToString()).Count;
                GameParametrs.OComboCounter += new Regex(oComboPattern).Matches(diagonal2.ToString()).Count;
                row.Clear();
                column.Clear();
                diagonal1.Clear();
                diagonal2.Clear();
            }
        }
    }
}
