﻿using System;

namespace ticTacToe.Handlers
{
    public static class CellSelection
    {
        public static void WASDCellSelect(int gridSize, out int row, out int column)
        {
            int x = gridSize - 1;
            int y = (gridSize - 1) / 2;
            Console.SetCursorPosition(x, y + 1);
            ConsoleKey key;
            while (true)
            {
                key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.Enter)
                {
                    break;
                }

                if (key == ConsoleKey.W && y > 0)
                {
                    y--;
                }

                if (key == ConsoleKey.S && y < gridSize - 1)
                {
                    y++;
                }

                if (key == ConsoleKey.A && x > 0)
                {
                    x -= 2;
                }

                if (key == ConsoleKey.D && x < gridSize * 2 - 2)
                {
                    x += 2;
                }

                Console.SetCursorPosition(x, y + 1);
            }
            row = y;
            column = x / 2;
        }
    }
}
