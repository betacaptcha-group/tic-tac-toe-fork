﻿namespace ticTacToe.Models
{
    public static class GameParametrs
    {
        public static int GridSize { get; set; }
        public static char[,] Cells { get; set; }
        public static int XComboCounter { get; set; } = 0;
        public static int OComboCounter { get; set; } = 0;
        public static char PlayerSign { get; set; }
        public static bool[,] IsCellFilled { get; set; }
        public static bool IsCrossFirst { get; set; }
        public static bool IsGameWithBot { get; set; }
        public const char xSign = 'x';
        public const char oSign = 'o';
    }
}
